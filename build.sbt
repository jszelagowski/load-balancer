name := "load-balancer"
version := "0.1"
scalaVersion := "2.13.2"

lazy val easyMockVersion = "4.2"
lazy val scalaTestVersion = "3.1.0"

libraryDependencies ++= Seq(
  "org.scalatest" %% "scalatest" % scalaTestVersion % Test,
  "org.easymock" % "easymock" % easyMockVersion  % Test,
  "org.scalatestplus" %% "easymock-3-2" % s"$scalaTestVersion.0"  % Test,
)
