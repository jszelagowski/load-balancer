package org.szelagowski.loadbalance

import java.util.UUID

import org.scalatest.concurrent.ScalaFutures
import org.szelagowski.loadbalance.balancer.LoadBalancer
import org.szelagowski.loadbalance.provider.Provider
import org.szelagowski.loadbalance.test.AbstractTest

import scala.concurrent.ExecutionContext.Implicits.global
import scala.concurrent.duration._
import scala.concurrent.{Future, Promise}

class ComponentTest extends AbstractTest with ScalaFutures {

  private val checkDelay = 200.millis

  it should "load balance, include and exclude providers" in {
    val (provider1, provider2) = (new TestProvider, new TestProvider)

    val config = LoadBalancerConfig(checkFrequency = checkDelay, reIncludeAfterSuccessfulChecks = 2, maxRequestsPerProvider = 5)
    val (balancer, checkControl) = LoadBalancerFactory.roundRobin(List(provider1, provider2), config)
      .fold(fail(_), identity)
    checkControl.init()

    getFewValuesWithDelay(balancer) should be(Set(provider1.id, provider2.id))

    provider2.isActive = false
    delayOfSingleCheck()
    getFewValuesWithDelay(balancer) should be(Set(provider1.id))

    provider1.isActive = false
    delayOfSingleCheck()
    expectFewFailuresWithDelay(balancer)

    provider2.isActive = true
    delayOfSingleCheck()
    expectNoProviderFailure(balancer.get())

    delayOfSingleCheck()
    balancer.get().futureValue should be(provider2.id)

    checkControl.cancel()
  }

  it should "throttle requests" in {
    val blockingProvider = new BlockingProvider
    val config = LoadBalancerConfig(checkFrequency = checkDelay, reIncludeAfterSuccessfulChecks = 2, maxRequestsPerProvider = 5)
    val (balancer, checkControl) = LoadBalancerFactory.roundRobin(List(blockingProvider), config)
      .getOrElse(fail("no balancer"))
    checkControl.init()

    val futures = (1 to config.maxRequestsPerProvider).map(_ => balancer.get())

    // new requests should fail
    balancer.get().failed.futureValue.getMessage should include("No free providers available")

    blockingProvider.promise.success(())
    futures.foreach(_.futureValue) // await completion
    Thread.sleep(20)

    // should be ready again
    balancer.get().futureValue
  }

  private def getFewValuesWithDelay(balancer: LoadBalancer) =
    (1 to 20).map { _ =>
      Thread.sleep(checkDelay.toMillis / 5)
      balancer.get().futureValue
    }.toSet

  private def expectFewFailuresWithDelay(balancer: LoadBalancer) =
    (1 to 20).map { _ =>
      Thread.sleep(checkDelay.toMillis / 5)
      expectNoProviderFailure(balancer.get())
    }.toSet

  private def expectNoProviderFailure(value: Future[String]) =
    value.failed.futureValue.getMessage should include("No free providers available")

  private def delayOfSingleCheck(): Unit = Thread.sleep(checkDelay.toMillis + 10)

  private class TestProvider extends Provider {
    val id: String = UUID.randomUUID().toString
    var isActive: Boolean = true

    override def get(): Future[String] = Future.successful(id)

    override def check(): Boolean = isActive
  }

  private class BlockingProvider extends TestProvider {
    val promise: Promise[Unit] = Promise[Unit]()

    override def get(): Future[String] = promise.future.map(_ => id)
  }

}
