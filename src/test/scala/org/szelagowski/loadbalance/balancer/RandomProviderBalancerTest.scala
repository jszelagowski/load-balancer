package org.szelagowski.loadbalance.balancer

import org.scalatest.concurrent.ScalaFutures
import org.szelagowski.loadbalance.provider.Provider
import org.szelagowski.loadbalance.test.AbstractTest

import scala.concurrent.Future
import scala.util.Random

class RandomProviderBalancerTest extends AbstractTest with ScalaFutures {

  private val (provider1, provider2) = (mock[Provider], mock[Provider])
  private val (id1, id2) = (Random.nextString(1000), Random.nextString(1000))

  private val balancer = new RandomProviderBalancer(List(provider1, provider2))

  it should "proxy get call to providers" in {
    expecting {
      provider1.get().andReturn(Future.successful(id1)).anyTimes()
      provider2.get().andReturn(Future.successful(id2)).anyTimes()
    }
    whenExecuting() {
      (1 to 20).map(_ => balancer.get().futureValue).toSet should be(Set(id1, id2))
    }
  }

  it should "handle empty list of providers" in {
    val balancer = new RandomProviderBalancer(Nil)
    balancer.get().failed.futureValue.getMessage should include("No provider available")
  }

  it should "create new balancer with additional provider" in {
    val provider3 = mock[Provider]
    val id3 = Random.nextString(1000)
    expecting {
      provider1.get().andReturn(Future.successful(id1)).anyTimes()
      provider2.get().andReturn(Future.successful(id2)).anyTimes()
      provider3.get().andReturn(Future.successful(id3)).anyTimes()
    }
    whenExecuting() {
      (1 to 20).map(_ => balancer.get().futureValue).toSet should be(Set(id1, id2))
      val newBalancer = balancer.withAdditional(provider3)
      (1 to 20).map(_ => newBalancer.get().futureValue).toSet should be(Set(id1, id2, id3))
    }
  }

  it should "create new balancer with excluded provider" in {
    expecting {
      provider1.get().andReturn(Future.successful(id1)).anyTimes()
      provider2.get().andReturn(Future.successful(id2)).anyTimes()
    }
    whenExecuting() {
      val balancer = new RandomProviderBalancer(List(provider1, provider2))
      (1 to 20).map(_ => balancer.get().futureValue).toSet should be(Set(id1, id2))
      val newBalancer = balancer.withExcluded(provider1)
      (1 to 20).map(_ => newBalancer.get().futureValue).toSet should be(Set(id2))
    }
  }

  it should "return number of active providers" in {
    balancer.activeProviders should be(2)
  }
}
