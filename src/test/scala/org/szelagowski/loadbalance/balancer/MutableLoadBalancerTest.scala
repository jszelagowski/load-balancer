package org.szelagowski.loadbalance.balancer

import org.szelagowski.loadbalance.provider.Provider
import org.szelagowski.loadbalance.test.AbstractTest

import scala.concurrent.Future
import scala.util.Random

class MutableLoadBalancerTest extends AbstractTest {

  it should "proxy get calls to underlying balancer" in {
    val underlying = mock[AdjustingLoadBalancer]
    val expectedValue = Future.successful(Random.nextString(100))
    expecting {
      underlying.get().andReturn(expectedValue).once()
    }
    whenExecuting() {
      val balancer = new MutableLoadBalancer(underlying)
      balancer.get() should be(expectedValue)
    }
  }

  it should "proxy active providers calls to underlying load balancer" in {
    val underlying = mock[AdjustingLoadBalancer]
    val balancer = new MutableLoadBalancer(underlying)
    val expectedValue = Random.nextInt(100)
    expecting {
      underlying.activeProviders.andReturn(expectedValue).once()
    }
    whenExecuting() {
      balancer.activeProviders should be(expectedValue)
    }
  }

  it should "swap underlying balancer using when include is called" in {
    val (initial, second) = (mock[AdjustingLoadBalancer], mock[AdjustingLoadBalancer])
    val expectedValue = Future.successful(Random.nextString(100))
    val additionalProvider = mock[Provider]
    expecting {
      initial.withAdditional(additionalProvider).andReturn(second).once()
      second.get().andReturn(expectedValue).once()
    }
    whenExecuting() {
      val balancer = new MutableLoadBalancer(initial)
      balancer.include(additionalProvider)
      balancer.get() should be(expectedValue)
    }
  }

  it should "swap underlying balancer using when exclude is called" in {
    val (initial, second) = (mock[AdjustingLoadBalancer], mock[AdjustingLoadBalancer])
    val expectedValue = Future.successful(Random.nextString(100))
    val additionalProvider = mock[Provider]
    expecting {
      initial.withExcluded(additionalProvider).andReturn(second).once()
      second.get().andReturn(expectedValue).once()
    }
    whenExecuting() {
      val balancer = new MutableLoadBalancer(initial)
      balancer.exclude(additionalProvider)
      balancer.get() should be(expectedValue)
    }
  }
}
