package org.szelagowski.loadbalance.balancer

import org.scalatest.concurrent.ScalaFutures
import org.szelagowski.loadbalance.provider.Provider
import org.szelagowski.loadbalance.test.AbstractTest

import scala.concurrent.Future
import scala.util.Random

class RoundRobinLoadBalancerTest extends AbstractTest with ScalaFutures {

  private val (provider1, provider2) = (mock[Provider], mock[Provider])
  private val (id1, id2) = (Random.nextString(1000), Random.nextString(1000))

  private val balancer = new RoundRobinLoadBalancer(Vector(provider1, provider2))

  it should "proxy get call to each provider one by one" in {
    expecting {
      provider1.get().andReturn(Future.successful(id1)).anyTimes()
      provider2.get().andReturn(Future.successful(id2)).anyTimes()
    }
    whenExecuting() {
      balancer.get().futureValue should be(id1)
      balancer.get().futureValue should be(id2)
      balancer.get().futureValue should be(id1)
      balancer.get().futureValue should be(id2)
      balancer.get().futureValue should be(id1)
      balancer.get().futureValue should be(id2)
      balancer.get().futureValue should be(id1)
    }
  }

  it should "handle empty list of providers" in {
    val balancer = new RoundRobinLoadBalancer(Vector.empty)
    balancer.get().failed.futureValue.getMessage should include("No provider available")
  }

  it should "create new balancer with additional provider and continue round robin where it left" in {
    val provider3 = mock[Provider]
    val id3 = Random.nextString(1000)
    expecting {
      provider1.get().andReturn(Future.successful(id1)).anyTimes()
      provider2.get().andReturn(Future.successful(id2)).anyTimes()
      provider3.get().andReturn(Future.successful(id3)).anyTimes()
    }
    whenExecuting() {
      balancer.get().futureValue should be(id1)
      balancer.get().futureValue should be(id2)
      balancer.get().futureValue should be(id1)
      val newBalancer = balancer.withAdditional(provider3)
      newBalancer.get().futureValue should be(id2)
      newBalancer.get().futureValue should be(id3)
      newBalancer.get().futureValue should be(id1)
      newBalancer.get().futureValue should be(id2)
      newBalancer.get().futureValue should be(id3)
    }
  }

  it should "create new balancer with excluded provider" in {
    expecting {
      provider1.get().andReturn(Future.successful(id1)).anyTimes()
      provider2.get().andReturn(Future.successful(id2)).anyTimes()
    }
    whenExecuting() {
      balancer.get().futureValue should be(id1)
      balancer.get().futureValue should be(id2)
      balancer.get().futureValue should be(id1)

      val excludedSecond = balancer.withExcluded(provider2)
      excludedSecond.get().futureValue should be(id1)
      excludedSecond.get().futureValue should be(id1)

      val excludedFirst = balancer.withExcluded(provider1)
      excludedFirst.get().futureValue should be(id2)
      excludedFirst.get().futureValue should be(id2)

      val excludedBoth = excludedFirst.withExcluded(provider2)
      excludedBoth.get().failed.futureValue.getMessage should include("No provider available")
    }
  }

  it should "return number of active providers" in {
    balancer.activeProviders should be(2)
  }
}
