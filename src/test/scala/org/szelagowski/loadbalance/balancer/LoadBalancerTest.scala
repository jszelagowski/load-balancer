package org.szelagowski.loadbalance.balancer

import org.szelagowski.loadbalance.provider.Provider
import org.szelagowski.loadbalance.test.AbstractTest

class LoadBalancerTest extends AbstractTest {

  List(
    (LoadBalancer.randomProvider _, "random provider load balancer"),
    (LoadBalancer.roundRobin _, "round robin load balancer"),
  ).foreach { case (factory, msg) =>

    msg should "not accept empty list of providers" in {
      factory(Nil) should be(Left(LoadBalancer.ListEmptyError))
    }

    msg should s"not accept list of providers greater than ${LoadBalancer.MaxSize}" in {
      val providers = (1 to (LoadBalancer.MaxSize + 1)).map(_ => mock[Provider]).toList
      factory(providers) should be(Left(LoadBalancer.ListExceedsSizeError))
    }
  }
}
