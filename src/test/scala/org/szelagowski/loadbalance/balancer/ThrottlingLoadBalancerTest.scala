package org.szelagowski.loadbalance.balancer

import org.scalatest.concurrent.ScalaFutures
import org.szelagowski.loadbalance.test.AbstractTest

import scala.concurrent.ExecutionContext.Implicits.global
import scala.concurrent.Promise
import scala.util.Random

class ThrottlingLoadBalancerTest extends AbstractTest with ScalaFutures {

  private val (limit, numberOfProviders) = (3, 2)
  private val underlying = mock[LoadBalancer]
  private val balancer = new ThrottlingLoadBalancer(underlying, limit)

  it should "should pass request within the limit and fail additional request" in {

    val completion = Promise[String]()

    expecting {
      underlying.get().andReturn(completion.future).anyTimes()
      underlying.activeProviders.andReturn(numberOfProviders).anyTimes()
    }
    whenExecuting() {
      val futures = (1 to limit * numberOfProviders).map(_ => balancer.get())

      // new gets should fail
      balancer.get().failed.futureValue.getMessage should include("No free providers available")

      completion.success("Success")
      futures.foreach(_.futureValue) // await completion
      Thread.sleep(20)

      // should be ready again
      balancer.get().futureValue
    }
  }

  it should "proxy active providers calls to underlying load balancer" in {
    val expectedValue = Random.nextInt(100)
    expecting {
      underlying.activeProviders.andReturn(expectedValue).once()
    }
    whenExecuting() {
      balancer.activeProviders should be(expectedValue)
    }
  }
}