package org.szelagowski.loadbalance.balancer

import org.easymock.EasyMock
import org.szelagowski.loadbalance.balancer.CheckingLoadBalancerTest.LoadBalancerWithManagement
import org.szelagowski.loadbalance.provider.{Provider, ProviderChecker}
import org.szelagowski.loadbalance.test.AbstractTest

import scala.concurrent.Future
import scala.util.Random

class CheckingLoadBalancerTest extends AbstractTest {
  private val underlying = mock[LoadBalancerWithManagement]
  private val checkerFactory = mock[ProviderChecker.Factory]
  private val checker = mock[ProviderChecker]

  private val onSuccess = EasyMock.newCapture[Provider => Unit]
  private val onFailure = EasyMock.newCapture[Provider => Unit]

  private val reIncludeAfter = 3
  private val balancer = new CheckingLoadBalancer(underlying, checkerFactory, reIncludeAfter)

  it should "delegate get to underlying load balancer" in {
    val expectedValue = Future.successful(Random.nextString(100))
    expecting {
      underlying.get().andReturn(expectedValue).once()
    }
    whenExecuting() {
      balancer.get() should be(expectedValue)
    }
  }

  it should "proxy active providers calls to to underlying load balancer" in {
    val expectedValue = Random.nextInt(100)
    expecting {
      underlying.activeProviders.andReturn(expectedValue).once()
    }
    whenExecuting() {
      balancer.activeProviders should be(expectedValue)
    }
  }

  it should "exclude provider on failed check" in {
    val excludedProvider = mock[Provider]
    expecting {
      checkerFactory(EasyMock.capture(onSuccess), EasyMock.capture(onFailure)).andReturn(checker).once()
      checker.init().once()
      underlying.exclude(excludedProvider)
    }
    whenExecuting() {
      balancer.init()

      onFailure.getValue.apply(excludedProvider)
    }
  }

  it should "not re-include provider if never excluded" in {
    val provider = mock[Provider]
    expecting {
      checkerFactory(EasyMock.capture(onSuccess), EasyMock.capture(onFailure)).andReturn(checker).once()
      checker.init().once()
    }
    whenExecuting() {
      balancer.init()

      (1 to (reIncludeAfter * 3)).foreach { _ =>
        onSuccess.getValue.apply(provider)
      }
    }
  }

  it should "not re-include provider if count not reached" in {
    val provider = mock[Provider]
    expecting {
      checkerFactory(EasyMock.capture(onSuccess), EasyMock.capture(onFailure)).andReturn(checker).once()
      checker.init().once()
      underlying.exclude(provider)
    }
    whenExecuting() {
      balancer.init()
      onFailure.getValue.apply(provider)

      (1 until reIncludeAfter).foreach { _ =>
        onSuccess.getValue.apply(provider)
      }
    }
  }

  it should "re-include provider when min count reached" in {
    val provider = mock[Provider]
    expecting {
      checkerFactory(EasyMock.capture(onSuccess), EasyMock.capture(onFailure)).andReturn(checker).once()
      checker.init().once()
      underlying.exclude(provider)
      underlying.include(provider)
    }
    whenExecuting() {
      balancer.init()
      onFailure.getValue.apply(provider)

      (1 to reIncludeAfter).foreach { _ =>
        onSuccess.getValue.apply(provider)
      }
    }
  }

  it should "not re-include provider when already re-included" in {
    val provider = mock[Provider]
    expecting {
      checkerFactory(EasyMock.capture(onSuccess), EasyMock.capture(onFailure)).andReturn(checker).once()
      checker.init().once()
      underlying.exclude(provider)
      underlying.include(provider)
    }
    whenExecuting() {
      balancer.init()
      onFailure.getValue.apply(provider)

      (1 to reIncludeAfter * 3).foreach { _ =>
        onSuccess.getValue.apply(provider)
      }
    }
  }

  it should "cancel checker" in {
    expecting {
      checkerFactory(EasyMock.anyObject(), EasyMock.anyObject()).andReturn(checker).once()
      checker.cancel().once()
    }
    whenExecuting() {
      balancer.cancel()
    }
  }
}

object CheckingLoadBalancerTest {

  // necessary for mocking
  trait LoadBalancerWithManagement extends LoadBalancer with LoadBalancerManagement

}
