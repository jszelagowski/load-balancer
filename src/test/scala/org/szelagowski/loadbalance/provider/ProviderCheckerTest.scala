package org.szelagowski.loadbalance.provider

import org.szelagowski.loadbalance.test.AbstractTest

import scala.concurrent.duration._
import scala.concurrent.{Await, Promise}

class ProviderCheckerTest extends AbstractTest {

  private val (provider1, provider2) = (mock[Provider], mock[Provider])
  private val onSuccess = mock[Provider => ()]
  private val onFailure = mock[Provider => ()]

  it should "call checks and report failed provider" in {
    val promise = Promise[Unit]()
    expecting {
      provider1.check().andReturn(true).anyTimes()
      provider2.check()
        .andReturn(true)
        .andReturn(true)
        .andReturn(false)
      provider2.check().andReturn(true).anyTimes()

      onSuccess(provider1).andReturn(()).times(3, 50)
      onSuccess(provider2).andReturn(()).times(2, 50)
      onFailure(provider2).andAnswer(() => promise.success(())).once()
    }
    whenExecuting() {
      val checker = new ProviderChecker(List(provider1, provider2), onSuccess, onFailure, 50.millis)
      checker.init()
      Await.result(promise.future, 600.millis)
    }
  }

  it should "not performed more checks when cancelled" in {
    expecting {
      provider1.check().andReturn(true).once()
      provider2.check().andReturn(true).once()
      onSuccess(provider1).andReturn(()).once()
      onSuccess(provider2).andReturn(()).once()
    }
    whenExecuting() {
      val checker = new ProviderChecker(List(provider1, provider2), onSuccess, onFailure, 20.millis)
      checker.init()
      checker.cancel()
      Thread.sleep(100)
    }
  }
}
