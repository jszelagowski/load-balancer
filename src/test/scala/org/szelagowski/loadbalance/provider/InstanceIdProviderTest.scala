package org.szelagowski.loadbalance.provider

import org.scalatest.concurrent.ScalaFutures
import org.szelagowski.loadbalance.test.AbstractTest

import scala.util.Random

class InstanceIdProviderTest extends AbstractTest with ScalaFutures {

  it should "get the instance id on consecutive calls" in {
    val id = Random.nextString(100);
    val provider = new InstanceIdProvider(id)
    provider.get().futureValue should be(id)
    provider.get().futureValue should be(id)
    provider.get().futureValue should be(id)
  }

  it should "get the instance id generated automatically on creation" in {
    val provider = new InstanceIdProvider
    val firstGet = provider.get().futureValue
    provider.get().futureValue should be(firstGet)
    provider.get().futureValue should be(firstGet)
  }

  it should "always be checked positively" in {
    val provider = new InstanceIdProvider()
    provider.check() should be(true)
  }
}
