package org.szelagowski.loadbalance.provider

import scala.concurrent.Future

trait Provider {
  def get(): Future[String]

  def check(): Boolean
}
