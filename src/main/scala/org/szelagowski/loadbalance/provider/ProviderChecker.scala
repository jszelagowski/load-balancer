package org.szelagowski.loadbalance.provider

import java.util.concurrent.ScheduledThreadPoolExecutor

import org.szelagowski.loadbalance.balancer.ChecksControl

import scala.concurrent.duration.FiniteDuration

class ProviderChecker(providers: List[Provider], onSuccess: Provider => (), onFailure: Provider => (), frequency: FiniteDuration)
  extends ChecksControl {

  private val scheduler = new ScheduledThreadPoolExecutor(1)
  scheduler.setExecuteExistingDelayedTasksAfterShutdownPolicy(false)

  override def init(): Unit = runChecksAndSchedule()

  override def cancel(): Unit = synchronized(scheduler.shutdown())

  private def runChecksAndSchedule(): Unit = {
    providers.foreach(checkProvider)
    schedule()
  }

  private def schedule(): Unit = synchronized {
    if (!scheduler.isShutdown) {
      scheduler.schedule(new Runnable {
        override def run(): Unit = runChecksAndSchedule()
      }, frequency.length, frequency.unit)
    }
  }

  private def checkProvider(provider: Provider): Unit =
    if (!provider.check()) onFailure(provider)
    else onSuccess(provider)
}

object ProviderChecker {

  trait Factory {
    def apply(onSuccess: Provider => Unit, onFailure: Provider => Unit): ProviderChecker
  }

  def factory(providers: List[Provider], frequency: FiniteDuration): Factory =
    (onSuccess, onFailure) => new ProviderChecker(providers, onSuccess = onSuccess, onFailure = onFailure, frequency)
}