package org.szelagowski.loadbalance.provider

import java.util.UUID

import scala.concurrent.Future

class InstanceIdProvider(instanceId: String = UUID.randomUUID().toString) extends Provider {
  override def get(): Future[String] = Future.successful(instanceId)

  override def check(): Boolean = true
}
