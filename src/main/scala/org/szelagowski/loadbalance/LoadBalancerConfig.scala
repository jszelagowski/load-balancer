package org.szelagowski.loadbalance

import scala.concurrent.duration.FiniteDuration

case class LoadBalancerConfig
(
  checkFrequency: FiniteDuration,
  reIncludeAfterSuccessfulChecks: Int,
  maxRequestsPerProvider: Int
)
