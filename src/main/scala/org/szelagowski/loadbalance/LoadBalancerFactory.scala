package org.szelagowski.loadbalance

import org.szelagowski.loadbalance.balancer.LoadBalancer.InitializationError
import org.szelagowski.loadbalance.balancer._
import org.szelagowski.loadbalance.provider.{Provider, ProviderChecker}

import scala.concurrent.ExecutionContext.Implicits.global

object LoadBalancerFactory {

  def randomProvider(providers: List[Provider], config: LoadBalancerConfig): Either[InitializationError, (LoadBalancer, ChecksControl)] =
    create(LoadBalancer.randomProvider)(providers, config)

  def roundRobin(providers: List[Provider], config: LoadBalancerConfig): Either[InitializationError, (LoadBalancer, ChecksControl)] =
    create(LoadBalancer.roundRobin)(providers, config)

  private def create(factory: List[Provider] => Either[InitializationError, LoadBalancer with LoadBalancerManagement])(providers: List[Provider], config: LoadBalancerConfig): Either[InitializationError, (LoadBalancer, ChecksControl)] =
    factory(providers)
      .map(balancer => new CheckingLoadBalancer(balancer, ProviderChecker.factory(providers, config.checkFrequency), config.reIncludeAfterSuccessfulChecks))
      .map { checkingBalancer =>
        val throttling = new ThrottlingLoadBalancer(checkingBalancer, config.maxRequestsPerProvider)
        (throttling, checkingBalancer)
      }
}
