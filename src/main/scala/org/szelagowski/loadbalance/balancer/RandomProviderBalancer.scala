package org.szelagowski.loadbalance.balancer

import org.szelagowski.loadbalance.provider.Provider

import scala.concurrent.Future
import scala.util.Random

protected class RandomProviderBalancer(providers: List[Provider]) extends LoadBalancer with AdjustingLoadBalancer {
  // the list is immutable so no concurrency issues
  // performance explicitly not prioritized in favor of code simplicity
  override def get(): Future[String] =
    Random.shuffle(providers).headOption
      .map(_.get())
      .getOrElse(Future.failed(new RuntimeException("No provider available")))

  override def withAdditional(provider: Provider): RandomProviderBalancer =
    new RandomProviderBalancer(providers :+ provider)

  override def withExcluded(provider: Provider): RandomProviderBalancer =
    new RandomProviderBalancer(providers.filterNot(_ == provider))

  override def activeProviders: Int = providers.size
}
