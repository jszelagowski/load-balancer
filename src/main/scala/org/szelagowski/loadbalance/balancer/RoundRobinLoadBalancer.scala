package org.szelagowski.loadbalance.balancer

import org.szelagowski.loadbalance.provider.Provider

import scala.concurrent.Future

protected class RoundRobinLoadBalancer(providers: Vector[Provider], firstIndex: Int = 0)
  extends LoadBalancer with AdjustingLoadBalancer {

  private val circular = (firstIndex until providers.size).iterator ++ Iterator.continually(providers.indices).flatten

  private def nextIndex = circular.synchronized(circular.next())

  override def get(): Future[String] =
    Some(providers).filter(_.nonEmpty)
      .flatMap(_.lift(nextIndex))
      .map(_.get())
      .getOrElse(Future.failed(new RuntimeException("No provider available")))

  override def withAdditional(provider: Provider): RoundRobinLoadBalancer =
    new RoundRobinLoadBalancer(providers :+ provider, if (providers.nonEmpty) nextIndex else 0)

  override def withExcluded(provider: Provider): RoundRobinLoadBalancer =
    new RoundRobinLoadBalancer(providers.filterNot(_ == provider), if (providers.nonEmpty) nextIndex else 0)

  override def activeProviders: Int = providers.size
}
