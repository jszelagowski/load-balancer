package org.szelagowski.loadbalance.balancer

import org.szelagowski.loadbalance.provider.Provider

trait AdjustingLoadBalancer extends LoadBalancer {
  def withAdditional(provider: Provider): AdjustingLoadBalancer

  def withExcluded(provider: Provider): AdjustingLoadBalancer
}
