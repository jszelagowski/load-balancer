package org.szelagowski.loadbalance.balancer

import org.szelagowski.loadbalance.provider.{Provider, ProviderChecker}

import scala.concurrent.Future

class CheckingLoadBalancer
(underlying: LoadBalancer with LoadBalancerManagement, checkerFactory: ProviderChecker.Factory, reIncludeAfterSuccessful: Int)
  extends LoadBalancer with ChecksControl {

  private lazy val checker = checkerFactory(onSuccess = checkSuccessful, onFailure = exclude)
  private var successfulChecksByExcludedProviders: Map[Provider, Int] = Map.empty

  override def get(): Future[String] = underlying.get()

  override def init(): Unit = checker.init()

  override def cancel(): Unit = checker.cancel()

  private def exclude(provider: Provider): Unit = {
    underlying.exclude(provider)
    successfulChecksByExcludedProviders = successfulChecksByExcludedProviders + (provider -> 0)
  }

  private def include(provider: Provider): Unit = {
    underlying.include(provider)
    successfulChecksByExcludedProviders = successfulChecksByExcludedProviders - provider
  }

  private def checkSuccessful(provider: Provider): Unit =
    successfulChecksByExcludedProviders.get(provider).foreach { count: Int =>
      if (count + 1 < reIncludeAfterSuccessful) successfulChecksByExcludedProviders = successfulChecksByExcludedProviders + (provider -> (count + 1))
      else include(provider)
    }

  override def activeProviders: Int = underlying.activeProviders
}
