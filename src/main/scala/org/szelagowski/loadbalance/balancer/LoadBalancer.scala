package org.szelagowski.loadbalance.balancer

import org.szelagowski.loadbalance.provider.Provider

import scala.concurrent.Future

trait LoadBalancer extends ProvidersCount {
  def get(): Future[String]
}

object LoadBalancer {
  type InitializationError = String

  val MaxSize = 10
  val ListEmptyError = "Providers list can not be empty"
  val ListExceedsSizeError = s"Providers list can not contain more than $MaxSize providers"

  def randomProvider(providers: List[Provider]): Either[InitializationError, LoadBalancer with LoadBalancerManagement] =
    verify(providers)
      .map(new RandomProviderBalancer(_))
      .map(new MutableLoadBalancer(_))

  def roundRobin(providers: List[Provider]): Either[InitializationError, LoadBalancer with LoadBalancerManagement] =
    verify(providers).map(_.toVector)
      .map(new RoundRobinLoadBalancer(_))
      .map(new MutableLoadBalancer(_))

  private def verify(providers: List[Provider]) =
    Right(providers)
      .filterOrElse(_.nonEmpty, ListEmptyError)
      .filterOrElse(_.size <= MaxSize, ListExceedsSizeError)
}
