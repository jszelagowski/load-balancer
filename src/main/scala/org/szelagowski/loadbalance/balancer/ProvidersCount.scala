package org.szelagowski.loadbalance.balancer

trait ProvidersCount {
  def activeProviders: Int
}
