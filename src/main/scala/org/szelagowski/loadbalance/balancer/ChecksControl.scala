package org.szelagowski.loadbalance.balancer

trait ChecksControl {
  def init(): Unit

  def cancel(): Unit
}
