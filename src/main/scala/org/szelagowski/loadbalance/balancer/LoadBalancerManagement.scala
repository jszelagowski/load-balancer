package org.szelagowski.loadbalance.balancer

import org.szelagowski.loadbalance.provider.Provider

trait LoadBalancerManagement {
  def include(provider: Provider): Unit

  def exclude(provider: Provider): Unit
}
