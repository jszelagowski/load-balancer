package org.szelagowski.loadbalance.balancer

import org.szelagowski.loadbalance.provider.Provider

import scala.concurrent.Future

protected class MutableLoadBalancer(initial: AdjustingLoadBalancer) extends LoadBalancer with LoadBalancerManagement {
  private var underlying = initial

  override def get(): Future[String] = underlying.get()

  override def include(provider: Provider): Unit =
    underlying = underlying.withAdditional(provider)

  override def exclude(provider: Provider): Unit =
    underlying = underlying.withExcluded(provider)

  override def activeProviders: Int = underlying.activeProviders
}
