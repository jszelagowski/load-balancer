package org.szelagowski.loadbalance.balancer

import scala.concurrent.{ExecutionContext, Future}

class ThrottlingLoadBalancer(underlying: LoadBalancer, maxOngoingRequestPerProvider: Int)(implicit ec: ExecutionContext)
  extends LoadBalancer {
  private var ongoingRequests = 0

  override def get(): Future[String] =
    if (checkAndIncrement()) {
      val result = underlying.get()
      result.onComplete(_ => decrement())
      result
    } else {
      Future.failed(new RuntimeException("No free providers available"))
    }

  private def checkAndIncrement(): Boolean = synchronized {
    if (ongoingRequests < maxOngoingRequestPerProvider * underlying.activeProviders) {
      ongoingRequests += 1
      true
    } else {
      false
    }
  }

  private def decrement(): Unit = synchronized(ongoingRequests -= 1)

  override def activeProviders: Int = underlying.activeProviders
}
