
# Load balancer

A load balancer is a component that, once invoked, it distributes incoming requests to a list of
registered providers and returns the value obtained from one of the registered providers to the original
caller. For simplicity, we will consider both the load balancer, and the provider having a public method
named `get()`.

### Step 1 – Generate provider
Generate a Provider that, once invoked on his `get()` method, retrieves a unique identifier (string)
of the provider instance

### Step 2 – Register a list of providers
Register a list of provider instances to the LoadBalancer – the max number of accepted providers from
the load balancer is 10

### Step 3 – Random invocation
Develop an algorithm that, when invoking multiple times the LoadBalancer on its `get()` method, should
cause the random invocation of the `get()` method of any registered provider instance.

### Step 4 – Round Robin invocation
Develop an algorithm that, when invoking multiple times the LoadBalancer on its `get()` method, should
cause the round-robin (sequential) invocation of the `get()` method of the registered providers

### Step 5 – Manual node exclusion / inclusion
Develop the possibility to exclude/include a specific provider into the balancer

### Step 6 – Heart beat checker
The load balancer should invoke every X seconds each of its registered providers on a special method called
`check()` to discover if they are alive – if not, it should exclude the provider node from load balancing

### Step 7 – Improving Heart beat checker
If a node has been previously excluded from the balancing it should be re-included if it has successfully
been “heart beat checked” for 2 consecutive times

### Step 8 – Cluster Capacity Limit
Assuming that each provider can handle a maximum number of Y parallel requests, the Balancer should not accept
any further request when it has (Y * alive providers) incoming requests running simultaneously

# Solution

### Requirements

* jdk 8+
* sbt (any version) - [Scala Build Tool](https://www.scala-sbt.org/)

### Testing
```
sbt test
```

### Example usage
```scala
val config = LoadBalancerConfig(checkFrequency = 30.seconds, reIncludeAfterSuccessfulChecks = 2, maxRequestsPerProvider = 30)
val (balancer, checkControl) = LoadBalancerFactory.roundRobin(providers, config).getOrElse( /* throw or recover from initialization error */ )
checkControl.init()

val futureValue = balancer.get()
```
See `ComponentTest` for more inspiration.

# Assumptions / known limitations
The list contains additional assumptions to make specification more explicit. Most of
the assumptions are easy to lift / change / implement if necessary.

* The provider and load balancer `get()` method is returning a future containing the string value because
Step 8 requires providers to be asynchronous.
* The providers `check()` method is assumed non-failing and synchronous. If it is not a case its signature
should be changed to return a future, and the checker logic should be adjusted.
* With small change load balancer errors could be distinguished from provider errors either by future
failure type (dedicated exception) or more verbose return type, eg `Either[Error, Future[_]]`

# License
Load balancer
Copyright (C) 2020  Jakub Szelagowski

This program is free software: you can redistribute it and/or modify
it under the terms of the GNU General Public License as published by
the Free Software Foundation, either version 3 of the License, or
(at your option) any later version.

This program is distributed in the hope that it will be useful,
but WITHOUT ANY WARRANTY; without even the implied warranty of
MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
GNU General Public License for more details.

You should have received a copy of the GNU General Public License
along with this program.  If not, see <https://www.gnu.org/licenses/>.
